import React, { Component } from 'react'
import Todo_List from './Todo_List.js'
import './Todo.css'

class TodoForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			items: [],
			currentItem: {
				text: '',
				key: ''
			}
		}
	}

	handleInput = (e) => {
		this.setState({
			currentItem: {
				text: e.target.value,
				key: this.state.items.length
			}
		})
	}

	addItem = e => {
		e.preventDefault();
		const newItem = this.state.currentItem;
		console.log(newItem)
		if (newItem.text !== '') {
			const newItems = [...this.state.items, newItem]
			this.setState(
				{
					items: newItems,
					currentItem: {
						text: '',
						key: ''
					}
				})
		}
	}

	deleteItem = key => {
		const newList = this.state.items.filter(item =>
			item.key !== key);
		this.setState(
			{ items: newList }
		)
	}

	editItem = key => {
		const text = prompt('Edit the List', this.state.items[key].text);
		if (text === "") {
			alert(`List Can't be blank`)
		}
		else if (text !== null) {
			const items = this.state.items;
			items.map(item => {
				if (item.key === key) {
					item.text = text;
				}
			})
			this.setState({
				items: items
			})
		}

	}

	render() {
		return (
			<div>
				<h1>to-do ({this.state.items.length})</h1>
				<form class='todo-input' onSubmit={this.addItem}>
					<input type="text" value={this.state.currentItem.text} placeholder='Enter item' onChange={this.handleInput} />
					<button type='submit'>Add</button>
				</form>
				<Todo_List items={this.state.items} editItem={this.editItem} deleteItem={this.deleteItem} />
			</div >
		)

	}
}

export default TodoForm