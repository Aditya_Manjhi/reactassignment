import React from 'react'
import './Todo.css'

function TodoList(props) {
	const items = props.items;
	const listItems = items.map(
		item => {
			return <div key={item.key}>
				<p>{item.text}
					<span>
						<button onClick={() => props.editItem(item.key)}>Edit</button>
						<button onClick={() => props.deleteItem(item.key)}>Delete</button>
					</span>
				</p>
			</div>
		}
	)

	return (
		<div>{listItems}</div>
	)
}

export default TodoList