import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import todoForm from './src/screens/todoForm'

const navigator = createStackNavigator(
  {
    todo: todoForm
  },
  {
    initialRouteName: "todo",
    defaultNavigationOptions: {
      title: "Todo List"
    }
  }
);

export default createAppContainer(navigator);
