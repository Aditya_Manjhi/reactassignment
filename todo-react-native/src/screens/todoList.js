import React, { useState } from 'react'
import { View, Text, StyleSheet, FlatList, Button, TouchableOpacity } from 'react-native'

const TodoList = ({ list, delItem, editItem }) => {
	return (
		<View style={styles.completeView}>
			<FlatList
				keyExtractor={list => list.key}
				data={list}
				renderItem={({ item }) => {
					return (
						<View style={styles.parentView} >
							<Text style={styles.listView} >{item.listItem}</Text>

							<TouchableOpacity style={styles.editStyle} onPress={() => editItem(item)}>
								<Text style={styles.editText}>Edit</Text>
							</TouchableOpacity>

							<TouchableOpacity style={styles.deleteStyle} onPress={() => delItem(item.key)}>
								<Text style={styles.deleteText}>Delete</Text>
							</TouchableOpacity>
						</View>
					)
				}
				}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	completeView: {
		margin: 15,
	},
	parentView: {
		flexDirection: 'row',
		marginBottom: 20
	},
	listView: {
		borderWidth: 1,
		borderRadius: 5,
		width: 200,
		height: 25,
		paddingLeft: 10
	},
	deleteStyle: {
		width: 60,
		height: 25,
		backgroundColor: '#453d2b',
		borderRadius: 10,
		alignSelf: 'center'
	},
	editStyle: {
		width: 50,
		height: 25,
		backgroundColor: '#453d2b',
		borderRadius: 10,
		alignSelf: 'center',
		marginHorizontal: 10
	},

	editText: {
		color: 'white',
		paddingLeft: 10
	},

	deleteText: {
		color: 'white',
		paddingLeft: 10
	}
})

export default TodoList