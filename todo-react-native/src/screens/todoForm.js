import React, { useState } from 'react'
import { View, TextInput, StyleSheet, Text, TouchableOpacity, Modal, Button, Alert } from 'react-native'
import TodoList from './todoList'

const todoForm = () => {
	const [currentValue, setValue] = useState('')
	const [lists, setList] = useState([])
	const [modalVisible, setVisibility] = useState(false)
	const [updateItem, setupdateItem] = useState({})


	const addItem = () => {
		let l = lists.length
		if (currentValue === '') {
			alertMsg()
		}
		else {
			setList([...lists, { listItem: currentValue, key: l.toString() }])
			setValue('')
		}
	}

	const delItem = (key) => {
		console.log(key)
		const newList = lists.filter(item =>
			item.key !== key);
		setList(newList)
	}

	const editItem = (item) => {
		setVisibility(true);
		setupdateItem(item)
	}

	const updateList = () => {
		const items = [...lists]
		if (updateItem.listItem === '') {
			alertMsg()
		}
		else {
			items.map(item => {
				if (item.key === updateItem.key) {
					item.listItem = updateItem.listItem;
				}
			})
			setList(items)
		}
		setVisibility(false)
	}

	const alertMsg = () => {
		Alert.alert(
			"Alert",
			`Item can't be blank`,
			[
				{
					text: "Cancel",
					onPress: () => console.log("Cancel Pressed"),
					style: "cancel"
				},
				{ text: "OK", onPress: () => console.log("OK Pressed") }
			],
			{ cancelable: false }
		)

	}

	return (
		<View style={styles.todoView}>
			<TextInput style={styles.input}
				value={currentValue}
				onChangeText={text => setValue(text)}
			/>
			<TouchableOpacity style={styles.buttonStyle} onPress={addItem}>
				<Text style={styles.buttonText}>Add</Text>
			</TouchableOpacity>
			<TodoList list={lists} delItem={delItem} editItem={editItem} />
			<View style={styles.show}>
				<Modal
					animationType={"fade"}
					transparent={false}
					visible={modalVisible}
					onRequestClose={() => { console.log("Modal has been closed.") }}>
					<View style={styles.modal} >
						<TextInput style={{ ...styles.input, width: 200 }}
							value={updateItem.listItem}
							onChangeText={text => setupdateItem({ ...updateItem, listItem: text })}
						/>
						<TouchableOpacity style={styles.buttonStyle} onPress={updateList}>
							<Text style={styles.buttonText}>Ok</Text>
						</TouchableOpacity>
						<TouchableOpacity style={{ ...styles.buttonStyle, width: 80 }} onPress={() => setVisibility(false)}>
							<Text style={styles.buttonText}>Cancel</Text>
						</TouchableOpacity>
					</View>
				</Modal>
			</View>

		</View>
	)
}

const styles = StyleSheet.create({
	todoView: {
		margin: 15
	},
	input: {
		borderWidth: 1,
		borderColor: 'black',
		paddingLeft: 10,
		borderRadius: 5
	},

	buttonStyle: {
		width: 60,
		height: 25,
		backgroundColor: '#453d2b',
		marginVertical: 10,
		borderRadius: 10,
		alignSelf: 'center'
	},

	buttonText: {
		marginHorizontal: 15,
		paddingTop: 3,
		color: 'white'
	},
	modal: {
		backgroundColor: '#ebebeb',
		height: 300,
		width: '60%',
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#fff',
		marginTop: '40%',
		marginLeft: '25%',
		paddingTop: 20,
		paddingLeft: 10
	}
})

export default todoForm